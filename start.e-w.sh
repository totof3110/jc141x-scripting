#!/usr/bin/env bash
cd "$(dirname "$(readlink -f "$0")")" || exit
echo "Support can be provided on our Matrix channel.

Pain heals, chicks dig scars, Glory lasts forever!"

source "$PWD/actions.sh"

# terminal
[ $TERMINAL_OUTPUT = 0 ] && exec &>/dev/null

# dwarfs
[ $EXTRACT = 0 ] && dwarfs-mount || { dwarfs-extract; UNMOUNT=0; }
[ $UNMOUNT = 1 ] && trap jc141-cleanup EXIT INT SIGINT SIGTERM

# wine
export WINEPREFIX="$PWD/files/prefix"
export WINEDLLOVERRIDES="winemenubuilder.exe=d;mshtml=d;nvapi,nvapi64=n"
export WINE_LARGE_ADDRESS_AWARE=1
[ ! -d "$WINEPREFIX" ] && wine-initiate_prefix

# setup external vulkan translation
[ ! -f "$WINEPREFIX/vulkan.log" ] && wine-setup_external_vulkan
export DXVK_ENABLE_NVAPI=1

# Enable DXVK's Async Shader Pipeline for performance boost
# Use with caution, especially with anticheat stuff
# export DXVK_ASYNC=1

# game files + launch args
ROOT="$PWD/files/game-root"; CMD=( "$SYSWINE" "game.exe" "$@" )

# do NOT touch!
declare -a RUN

# gamescope
[ -x "$(command -v "gamescope")" ] && [ $GAMESCOPE = 1 ] && RUN+=( gamescope-run_embedded )

# isolation
[ $ISOLATE = 1 ] && { export ISOLATION_TYPE='wine'; RUN+=( bash 'actions.sh' bwrap-run_in_sandbox --chdir "$ROOT" ); } || cd "$ROOT"

# start
RUN+=( "${CMD[@]}" ); "${RUN[@]}"
